<?php

namespace Drupal\media_views_filter\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Media alt text field handler.
 *
 * @ViewsField("media_alt_text")
 *
 * @DCG
 * The plugin needs to be assigned to a specific table column through
 * hook_views_data() or hook_views_data_alter().
 * For non-existent columns (i.e. computed fields) you need to override
 * self::query() method.
 */
class MediaAltText extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Return media alt text.
    if ($values->_entity) {
      $media_entity = $values->_entity;
      return $media_entity->get('thumbnail')->getValue()[0]['alt'];
    }

    return NULL;
  }

}
