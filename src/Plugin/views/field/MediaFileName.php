<?php

namespace Drupal\media_views_filter\Plugin\views\field;

use Drupal\Core\Render\Markup;
use Drupal\file\Entity\File;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Media File Name field handler.
 *
 * @ViewsField("media_file_name")
 *
 * @DCG
 * The plugin needs to be assigned to a specific table column through
 * hook_views_data() or hook_views_data_alter().
 * For non-existent columns (i.e. computed fields) you need to override
 * self::query() method.
 */
class MediaFileName extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Return file name.
    if ($values->_entity) {
      $media_entity = $values->_entity;
      $file_entity_id = $media_entity->getSource()
        ->getSourceFieldValue($media_entity);
      if ($file_entity_id) {
        $file_entity = File::load($file_entity_id);
        if ($file_entity) {
          // We actually return the URL because the
          // filename is not always the actual filename
          // (file_managed table filename vs uri).
          return Markup::create('<a href="' . $file_entity->createFileUrl() . '" target="_blank">' . $file_entity->createFileUrl() . '</a>');
        }
      }
    }

    return NULL;
  }

}
