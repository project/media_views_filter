<?php

namespace Drupal\media_views_filter\Plugin\views\filter;

use Drupal\Core\Database\Connection;
use Drupal\views\Plugin\views\filter\StringFilter;
use Drupal\views\Plugin\ViewsHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter for searching Media and File strings.
 *
 * Including media name, file name, alt text attribute
 * and media title attribute.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("media_file_name")
 */
class MediaFileNameFilter extends StringFilter {

  /**
   * The Views Plugin Manager service.
   */
  protected ViewsHandlerManager $viewsHandlerManager;

  /**
   * Constructs a new MediaFileNameFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $views_handler_manager
   *   The Views Plugin Manager service.
   */
  public function __construct(array $configuration, string $plugin_id, mixed $plugin_definition, Connection $connection, ViewsHandlerManager $views_handler_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $connection);
    $this->viewsHandlerManager = $views_handler_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('plugin.manager.views.join')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();
    // Left join table file_usage to media_field_data.
    $join_1_def = [
      'table' => 'file_usage',
      'field' => 'id',
      'left_table' => 'media_field_data',
      'left_field' => 'mid',
    ];
    $join_1 = $this->viewsHandlerManager->createInstance('standard', $join_1_def);
    $this->query->addRelationship('file_usage', $join_1, 'file_usage');
    // Inner join table file_managed.
    $join_2_def = [
      'type' => 'INNER',
      'table' => 'file_managed',
      'field' => 'fid',
      'left_table' => 'file_usage',
      'left_field' => 'fid',
    ];
    $join_2 = $this->viewsHandlerManager->createInstance('standard', $join_2_def);
    $this->query->addRelationship('file_managed', $join_2, 'file_managed');

    // Remove duplicate results.
    // https://www.drupal.org/project/drupal/issues/2653508#comment-13826614
    $this->query->addField(NULL, 'media_field_data.mid', '', ['function' => 'groupby']);
    $this->query->addGroupBy('media_field_data.mid');
    // @todo Remove if fixes error filtering on acquia at /admin/content/media.
    $this->query->addGroupBy('media_field_data.changed');

    // In hook_views_data we assign this filter a table and field to filter on
    // but this adds the WHERE clause in a group that doesn't work
    // for our purpose. Since we will add it to the right group
    // below, we delete it.
    foreach ($this->query->where as $where_key => $where) {
      foreach ($where['conditions'] as $con_key => $condition) {
        if ($condition['field'] == 'media_field_data.name') {
          unset($this->query->where[$where_key]['conditions'][$con_key]);
        }
      }
    }

    // Add WHERE OR clauses for all the media and file strings
    // we want to filter on.
    $group_id = $this->query->setWhereGroup('OR');
    $this->query->addWhere($group_id, 'media_field_data.name', '%' . $this->connection->escapeLike($this->value) . '%', 'LIKE');
    // We want to filter on file_managed.uri as well as file_managed.filename.
    // file_managed.filename does not always contain the real filename
    // (don't ask me why). However, we want to avoid including the URI
    // scheme/stream so we use REPLACE.
    $placeholder = $this->placeholder();
    $value_placeholder = $placeholder . '_value';
    // @todo Pull all possible URI schemes from somewhere to avoid hardcoding them.
    $this->query
      ->addWhereExpression($group_id, "REPLACE(REPLACE(file_managed.uri, 'public://', ''), 'private://', '') LIKE $value_placeholder", [$value_placeholder => '%' . $this->connection->escapeLike($this->value) . '%']);
    $this->query->addWhere($group_id, 'file_managed.filename', '%' . $this->connection->escapeLike($this->value) . '%', 'LIKE');
    // @todo Determine if we want image alt and data field considered in this!
    $this->query->addWhere($group_id, 'media_field_data.thumbnail__alt', '%' . $this->connection->escapeLike($this->value) . '%', 'LIKE');
    $this->query->addWhere($group_id, 'media_field_data.thumbnail__title', '%' . $this->connection->escapeLike($this->value) . '%', 'LIKE');
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = parent::operators();

    // Unset the operators we don't want to allow,
    // which is everything except "contains".
    // @todo Why doesn't word and allwords return any results?
    // Seems like we should be able to enable those options.
    unset($operators['=']);
    unset($operators['!=']);
    unset($operators['word']);
    unset($operators['allwords']);
    unset($operators['starts']);
    unset($operators['not_starts']);
    unset($operators['ends']);
    unset($operators['not_ends']);
    unset($operators['shorterthan']);
    unset($operators['longerthan']);
    unset($operators['regular_expression']);
    unset($operators['empty']);
    unset($operators['not empty']);

    return $operators;
  }

}
